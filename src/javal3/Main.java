package javal3;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javal3.model.Picture;
import javal3.model.dao.PictureDAO;
import javal3.model.dao.PictureDAOXML;
import javal3.ui.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class Main extends Application {

    private Stage mainStage;
    private AnchorPane root;
    private MainLayoutController rootController;
    private ObservableList<Picture> imageslist;
    private PictureDAO dao;

    public static String password;

    public Main(){
        dao = new PictureDAOXML();
        dao.setAutoCommit(true);

        password = "";

        imageslist = FXCollections.observableArrayList();

    }

    public ObservableList<Picture> getImageslist(){
        return imageslist;
    }

    public Stage getStage(){
        return mainStage;
    }


    @Override
    public void start(Stage primaryStage){
        try {

            mainStage = primaryStage;
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("ui/PasswordEnterDialog.fxml"));
            root = (AnchorPane) loader.load();
            Scene scene  = new Scene(root);
            mainStage.setScene(scene);
            PasswordEnterDialogController passController = loader.getController();
            mainStage.setTitle("Gestionnaire d'images - Mot de Passe");

            /*
            loader.setLocation(getClass().getResource("ui/MainLayout.fxml"));
            mainStage.setTitle("Gestionnaire d'images");
            rootController.setPicturesList(imageslist);
            rootController.setAllPicture(imageslist);
            */

            passController.setMain(this);
            mainStage.show();
        }
        catch(IOException e){
            showErrorExceptionDialog(e);
        }
    }


    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Ouverture boite de dialogue pour afficher les exceptions éventuelles levées.
     * @param e l'exception levée à affichée.
     */
    public void showErrorExceptionDialog(Exception e){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Exception Dialog");
        alert.setHeaderText("Look, an Exception Dialog");
        alert.setContentText(e.getMessage());


        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);
        alert.showAndWait();
    }

    public void showMainLayout(){

        try {
            imageslist = FXCollections.observableArrayList(dao.getAll());
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("ui/MainLayout.fxml"));
            root = (AnchorPane) loader.load();
            Scene scene  = new Scene(root);
            mainStage.setScene(scene);
            rootController = loader.getController();
            mainStage.setTitle("Gestionnaire d'images");
            rootController.setPicturesList(imageslist);
            rootController.setAllPicture(imageslist);
            rootController.setMain(this);
            mainStage.show();
        }
        catch(IOException e){
            showErrorExceptionDialog(e);
        }

    }

    public void showImageFullLayout(int index_img){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("ui/ImageFullLayout.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            Scene scene = new Scene(layout);
            stage.setScene(scene);
            ImageFullLayoutController controller = loader.getController();
            controller.setStage(stage);
            controller.setImages(this.imageslist);
            controller.setMain(this);
            controller.changeImageDisplay(index_img);

            //Lorsque l'on ferme la fenêtre par la croix
            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    mainStage.show();
                }
            });

            //Lorsque l'on ferme la fenêtre par suppression de l'image ou par le menu
            stage.setOnHidden(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    mainStage.show();
                }
            });
            mainStage.hide();
            stage.show();

        }
        catch(Exception e){
            //showErrorExceptionDialog(e);
            e.printStackTrace();
        }

    }

    public boolean showImportImageDialog(Stage ownerStage){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("ui/ImportImageDialog.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(ownerStage);
            Scene scene = new Scene(layout);
            stage.setScene(scene);
            ImportImageDialogController controller = loader.getController();
            controller.setStage(stage);
            Picture p = null;
            controller.setImagesList(imageslist);
            controller.setMain(this);
            stage.setTitle("Import d'une image");
            stage.showAndWait();
            return controller.isValid();


        }
        catch(IOException e){
            showErrorExceptionDialog(e);
            return false;
        }
    }

    public void showTagAddDialog(Stage ownerStage, Picture p){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("ui/TagAddDialog.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(ownerStage);
            Scene scene = new Scene(layout);
            stage.setScene(scene);
            TagAddDialogController controller = loader.getController();
            controller.setStage(stage);
            controller.setPicture(p);
            stage.setTitle("Ajout d'un tag");
            stage.showAndWait();
        }
        catch(IOException e){
            showErrorExceptionDialog(e);
        }
    }

    public void showNameChangeDialog(Stage ownerStage, Picture p){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("ui/NameChangeDialog.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(ownerStage);
            Scene scene = new Scene(layout);
            stage.setScene(scene);
            NameChangeDialogController controller = loader.getController();
            controller.setStage(stage);
            controller.setPicture(p);
            stage.setTitle("Modification du nom");
            stage.showAndWait();
        }
        catch(IOException e) {
            showErrorExceptionDialog(e);
        }
    }

    public PictureDAO getDAO(){
        return dao;
    }

    public String getPassword(){
        return this.password;
    }

    public void setPassword(String password){
        this.password = password;
    }

}
