package javal3.model;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javal3.model.transformation.Transformation;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pierre on 26/04/2016.
 */
public class Picture {
    //Attributs
    private int id;
    private ObjectProperty<Image> img;
    private ObservableList<String> tags;
    private StringProperty nom;


    //Constructeurs
    public Picture(String url, String nom){
        this.img = new SimpleObjectProperty<>(new Image(url));
        this.nom = new SimpleStringProperty();

        if(!nom.equals("")) this.nom.set(nom);
        else{
            String tmp = new String(url).replace('\\', '/');
            String split[] = tmp.substring(0, tmp.lastIndexOf(".")).split("/");
            this.nom.set(split[split.length-1]);
        }
        this.tags = FXCollections.observableArrayList();
    }

    public Picture(String url, ArrayList<Transformation> transfos){

        this.img = new SimpleObjectProperty<>(new Image(url));
        this.nom = new SimpleStringProperty();

        String tmp = new String(url).replace('\\', '/');
        String split[] = tmp.substring(0, tmp.lastIndexOf(".")).split("/");
        this.nom.set(split[split.length-1]);

        this.tags = FXCollections.observableArrayList();

        for(int i = 0; i < transfos.size(); i++){
            applyTransformation(transfos.get(i));
        }

    }

    public Picture(String url, String nom, List<Transformation> transfos){

        this.img = new SimpleObjectProperty<>(new Image(url));
        this.nom = new SimpleStringProperty();

        if(!nom.equals("")) this.nom.set(nom);
        else{
            String tmp = new String(url).replace('\\', '/');
            String split[] = tmp.substring(0, tmp.lastIndexOf(".")).split("/");
            this.nom.set(split[split.length-1]);
        }

        this.tags = FXCollections.observableArrayList();

        for(int i = 0; i < transfos.size(); i++){
            applyTransformation(transfos.get(i));
        }

    }

    //Getters
    public int getId(){
        return id;
    }

    public Image getImage(){
        return this.img.get();
    }

    public ObservableList<String> getTags(){
        return this.tags;
    }

    public String getNom(){
        return this.nom.get();
    }

    public String getTagByIndex(int index){
        if(index < 0 || index >= tags.size())
            throw new IndexOutOfBoundsException();
        return this.tags.get(index);
    }
    public String getTagByNom(String nom){
        if(this.tags.contains(nom))
            return this.tags.get(this.tags.indexOf(nom));
        return null;
    }

    //Properties
    public ObjectProperty<Image> getImageProperty(){
        return img;
    }
    public StringProperty getNomProperty(){
        return nom;
    }

    //Setters
    public void setId(int id){
        this.id = id;
    }

    public void setNom(String nom){
        this.nom.set(nom);
    }

    public void setTags(ArrayList<String> tags){
        this.tags = FXCollections.observableArrayList();
        for(String tag : tags){
            addTag(tag);
        }
    }

    public void setImg(Image img){
        this.img.set(img);
    }

    //Méthodes
    public boolean addTag(String tag){
        if(!tags.contains(tag)){
            this.tags.add(tag);
            return true;
        }
        return false;

    }

    public void removeTag(int index){
        if(index < 0 || index >= tags.size())
            throw new IndexOutOfBoundsException();
        this.tags.remove(index);
    }

    public void removeTag(String tag){
        this.tags.remove(tag);
    }

    public void applyTransformation(Transformation t){
        this.img.set(t.transformationImage(this.img.get()));
    }


}
