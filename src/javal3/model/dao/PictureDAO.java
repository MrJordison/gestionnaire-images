package javal3.model.dao;

import javal3.model.Picture;

import java.util.List;

/**
 * Created by mrjordison on 11/05/16.
 */
public abstract class PictureDAO {

    public abstract void create(Picture p);

    public abstract boolean update(Picture p);

    public abstract boolean delete(Picture p);

    public abstract Picture search(int id);

    public abstract List<Picture> getAll();

    public abstract void applyTransform(String transfoType, Picture p);

    public abstract void deleteTransform(String transfoType, Picture p);

    public abstract void deleteAllTransform(Picture p);

    public abstract void setAutoCommit(boolean auto);

    public abstract void commit();
}
