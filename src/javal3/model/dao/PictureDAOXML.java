package javal3.model.dao;

import javafx.embed.swing.SwingFXUtils;
import javal3.model.Picture;
import javal3.model.transformation.Transformation;
import javal3.model.transformation.TransformationFactory;
import org.jdom2.Element;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.renderable.RenderableImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mrjordison on 12/05/16.
 */
public class PictureDAOXML extends PictureDAO {

    private XMLConnector connector;
    private boolean autoCommit;


    private Picture convertFromXML(Element picture){
        if(picture==null)
            return null;

        TransformationFactory tf = new TransformationFactory();
        List<Transformation> transfos = new ArrayList<>();
        for (Element e : picture.getChild("transformList").getChildren("transformation"))
            transfos.add(tf.getTransformation(e.getText()));

        //constructeur de Picture à partir de l'url, du nom et des transformations
        Picture res = new Picture(
                picture.getChild("url").getText(),
                picture.getChild("nom").getText(),
                transfos
        );
        //récupération de l'id
        res.setId(Integer.parseInt(picture.getAttributeValue("id")));

        //récupération des tags
        for(Element e :picture.getChild("tagsList").getChildren("tag"))
            res.addTag(e.getText());

        return res;

    }

    public PictureDAOXML(){
        connector = XMLConnector.getInstance();

        //par défaut, le fichier n'est pas mis à jour auto à la modification de l'arbo du xml
        autoCommit = false;
    }

    /**
     * Save d'une instance de Picture non présente dans le fichier xml et retourne celle ci avec son id
     * @param p
     */
    public void create(Picture p){
        int id=-1;
        try {
            id = Integer.parseInt(connector.getDocument().getRootElement().getAttributeValue("idcount")) + 1;
        }
        catch(Exception e){
            e.printStackTrace();
        }

        //création de l'élément racine de l'objet
        Element picture = new Element("picture");

        //met à jour l'id de l'instance de Picture(xml et objet)
        picture.setAttribute("id",""+id);
        p.setId(id);

        //ajout du nom
        Element nom = new Element("nom");
        nom.setText(p.getNom());
        picture.addContent(nom);

        //ajout de l'url de l'image
        Element url = new Element("url");
        url.setText(p.getImage().impl_getUrl().toString());
        picture.addContent(url);

        //ajout des tags
        Element tagsList = new Element("tagsList");
        for(String tag : p.getTags())
            tagsList.addContent(new Element("tag").setText(tag));
        picture.addContent(tagsList);

        //ajout d'une liste pour les transformation(vide à la création)
        picture.addContent(new Element("transformList"));

        //ajout de l'instance au document
        connector.getDocument().getRootElement().addContent(picture);
        connector.getDocument().getRootElement().setAttribute("idcount",""+(id));

        //auto commit si attribut à true
        if(autoCommit)
            commit();

    }

    public boolean update(Picture p){
        //création de la requête à envoyer
        String query = "//picture[@id='"+p.getId()+"']";
        XPathExpression<Element> xpe = XPathFactory.instance().compile(query, Filters.element());
        Element picture;
        try{
            picture = xpe.evaluate(connector.getDocument()).get(0);
        }
        catch(IndexOutOfBoundsException e){
            return false;
        }

        //maj du nom
        picture.getChild("nom").setText(p.getNom());

        //maj des tags
        Element tagsList = picture.getChild("tagsList");
        tagsList.removeChildren("tag");
        for(String tag : p.getTags())
            tagsList.addContent(new Element("tag").setText(tag));

        //auto commit si attribut à true
        if(autoCommit)
            commit();

        return true;
    }

    public boolean delete(Picture p){

        //création de la requête à envoyer
        String query = "//picture[@id='"+p.getId()+"']";
        XPathExpression<Element> xpe = XPathFactory.instance().compile(query, Filters.element());
        Element picture;
        try{
            picture = xpe.evaluate(connector.getDocument()).get(0);
        }
        catch(IndexOutOfBoundsException e){
            return false;
        }
        connector.getDocument().getRootElement().removeContent(picture);


        //auto commit si attribut à true
        if(autoCommit)
            commit();

        return true;

    }

    public Picture search(int id){

        //création de la requête à envoyer
        String query = "//picture[@id='"+id+"']";
        XPathExpression<Element> xpe = XPathFactory.instance().compile(query, Filters.element());
        try {
            return convertFromXML(xpe.evaluate(connector.getDocument()).get(0));
        }
        catch(IndexOutOfBoundsException e){
            return null;
        }

    }

    public List<Picture> getAll(){

        List<Picture> pictures = new ArrayList<>();
        for(Element e : connector.getDocument().getRootElement().getChildren("picture"))
            pictures.add(convertFromXML(e));

        return pictures;

    }

    public void applyTransform(String transfoType, Picture p){

        //récupère l'instance depuis le xml
        String query = "//picture[@id='" + p.getId() + "']";
        XPathExpression<Element> xpe = XPathFactory.instance().compile(query, Filters.element());
        Element picture = xpe.evaluate(connector.getDocument()).get(0);

        if(transfoType.equals("Securite")){

            System.out.println("GHYGUGUI");
            String url = picture.getChild("url").getValue();
            File folder = new File(url.substring(url.indexOf(":/")+2));
            BufferedImage bfi = SwingFXUtils.fromFXImage(p.getImage(), null);
            try {
                ImageIO.write(bfi, "png", folder);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        //passage du type de transfomation en xml et ajout au document
        Element transformation = new Element("transformation");
        transformation.setText(transfoType);
        picture.getChild("transformList").addContent(transformation);

        //auto commit si attribut à true
        if(autoCommit)
            commit();

    }

    public void deleteTransform(String transfoType, Picture p){

        //récupère l'instance depuis le xml
        String query = "//picture[@id='"+p.getId()+"']";
        XPathExpression<Element> xpe = XPathFactory.instance().compile(query, Filters.element());
        Element picture = xpe.evaluate(connector.getDocument()).get(0);

        //passage du type de transformation en xml et suppression
        Element transformation = new Element("transformation");
        transformation.setText(transfoType);
        picture.getChild("transformList").removeContent(transformation);

        //auto commit si attribut à true
        if(autoCommit)
            commit();

    }

    public void deleteAllTransform(Picture p){

        //récupère l'instance depuis le xml
        String query = "//picture[@id='"+p.getId()+"']";
        XPathExpression<Element> xpe = XPathFactory.instance().compile(query, Filters.element());
        Element picture = xpe.evaluate(connector.getDocument()).get(0);

        //suppression de toutes les transformations
        picture.getChild("transformList").removeContent();

        //auto commit si attribut à true
        if(autoCommit)
            commit();

    }

    public void commit(){
        connector.commit();
    }
    public void setAutoCommit(boolean auto){
        autoCommit = auto;
    }
}
