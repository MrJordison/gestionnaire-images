package javal3.model.dao;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by mrjordison on 14/05/16.
 */
public class XMLConnector {
    private static File metadata;
    private static Document document;

    private XMLConnector(){
        XMLConnectorHolder.instance.metadata = new File("resources/metadatas.xml");

        //création du dossier pour stocker les metadatas et la biblio d'images si celui ci n'existe pas
        XMLConnectorHolder.instance.metadata.getParentFile().mkdirs();

        //Si le fichier de metadatas n'existe pas, on l'initialise
        if(!XMLConnectorHolder.instance.metadata.exists()){
            Element root = new Element("bibliotheque");
            root.setAttribute("idcount","0");
            XMLConnectorHolder.instance.document = new Document(root);
        }

        //sinon il est chargé dans l'attribut document
        else try {
            XMLConnectorHolder.instance.document = new SAXBuilder().build(XMLConnectorHolder.instance.metadata);
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static class XMLConnectorHolder{
        private static final XMLConnector instance = new XMLConnector();
    }

    public static XMLConnector getInstance(){
        return XMLConnectorHolder.instance;
    }


    public Document getDocument(){
        return XMLConnectorHolder.instance.document;
    }

    public void commit(){
        XMLOutputter output = new XMLOutputter(Format.getPrettyFormat());
        try {
            output.output(XMLConnectorHolder.instance.document,
                    new FileOutputStream(XMLConnectorHolder.instance.metadata));
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

}
