package javal3.model.transformation;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * Created by Pierre on 02/05/2016.
 */
public class FiltreSobel extends Transformation{

    @Override
    public Image transformationImage(Image image){

        WritableImage wImage = new WritableImage((int)image.getWidth(), (int)image.getHeight());

        PixelReader pr = image.getPixelReader();
        PixelWriter pw = wImage.getPixelWriter();

        Color after;

        int[][] gx = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
        int[][] gy = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};

        for(int x = 1; x < wImage.getWidth()-2; x++){

            for(int y = 1; y < wImage.getHeight()-2; y++){

                Color[][] tmp = {
                        {pr.getColor(x-1, y-1), pr.getColor(x, y-1), pr.getColor(x+1, y-1)},
                        {pr.getColor(x-1, y), pr.getColor(x, y), pr.getColor(x+1, y)},
                        {pr.getColor(x-1, y+1), pr.getColor(x, y+1), pr.getColor(x+1, y+1)}};

                double redX = 0;
                double greenX = 0;
                double blueX = 0;
                double redY = 0;
                double greenY = 0;
                double blueY = 0;

                for(int e = 0; e < 3; e++){

                    for(int j = 0; j < 3; j++) {

                        redX += tmp[e][j].getRed() * gx[e][j];
                        greenX += tmp[e][j].getGreen() * gx[e][j];
                        blueX += tmp[e][j].getBlue() * gx[e][j];

                        redY += tmp[e][j].getRed() * gy[e][j];
                        greenY += tmp[e][j].getGreen() * gy[e][j];
                        blueY += tmp[e][j].getBlue() * gy[e][j];

                    }

                }

                double totalRed = Math.abs(redX)+Math.abs(redY);
                double totalGreen = Math.abs(greenX)+Math.abs(greenY);
                double totalBlue = Math.abs(blueX)+Math.abs(blueY);

                if(totalRed < 0)
                    totalRed = 0;
                if(totalRed > 1)
                    totalRed = 1;

                if(totalGreen < 0)
                    totalGreen = 0;
                if(totalGreen > 1)
                    totalGreen = 1;

                if(totalBlue < 0)
                    totalBlue = 0;
                if(totalBlue > 1)
                    totalBlue = 1;

                after = new Color(totalRed, totalGreen, totalBlue, pr.getColor(x, y).getOpacity());

                pw.setColor(x, y, after);

            }

        }

        return wImage;

    }

}
