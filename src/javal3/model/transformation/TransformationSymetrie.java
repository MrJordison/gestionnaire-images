package javal3.model.transformation;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * Created by Pierre on 02/05/2016.
 */
public class TransformationSymetrie extends Transformation {

    private boolean horizontal;

    public TransformationSymetrie(){
        this.horizontal = true;
    }

    public TransformationSymetrie(boolean horizontal){
        this.horizontal = horizontal;
    }

    @Override
    public Image transformationImage(Image image){

        WritableImage wImage = new WritableImage((int)image.getWidth(), (int)image.getHeight());
        PixelReader pr = image.getPixelReader();
        PixelWriter pw = wImage.getPixelWriter();
        Color before;

        for(int x = 0; x < image.getWidth(); x++){

            for(int y = 0; y < image.getHeight(); y++){

                before = pr.getColor(x, y);

                if(horizontal)
                    pw.setColor((int)image.getWidth()-x-1, y, before);
                else
                    pw.setColor(x, (int)image.getHeight()-y-1, before);

            }

        }

        return wImage;

    }

    public void setHorizontal(boolean horizontal){
        this.horizontal = horizontal;
    }
    public boolean isHorizontal(){
        return horizontal;
    }

}
