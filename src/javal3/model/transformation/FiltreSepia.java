package javal3.model.transformation;

import javafx.scene.image.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * Created by Pierre on 26/04/2016.
 */
public class FiltreSepia extends Transformation {

    @Override
    public Image transformationImage(Image image){

        WritableImage wImage = new WritableImage((int)image.getWidth(), (int)image.getHeight());
        PixelReader pr = image.getPixelReader();
        PixelWriter pw = wImage.getPixelWriter();
        Color before;
        Color after;

        for(int x = 0; x < image.getWidth(); x++){

            for(int y = 0; y < image.getHeight(); y++){

                before = pr.getColor(x, y);

                double r = (before.getRed() * .393) + (before.getGreen() *.769) + (before.getBlue() * .189);
                double g = (before.getRed() * .349) + (before.getGreen() *.686) + (before.getBlue() * .168);
                double b = (before.getRed() * .272) + (before.getGreen() *.534) + (before.getBlue() * .131);

                if(r > 1.0)
                    r = 1.0;
                if(g > 1.0)
                    g = 1.0;
                if(b > 1.0)
                    b = 1.0;

                after = new Color(r, g, b, before.getOpacity());
                pw.setColor(x, y, after);

            }

        }

        return wImage;

    }

}
