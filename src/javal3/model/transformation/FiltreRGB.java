package javal3.model.transformation;

import javafx.scene.image.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * Created by Pierre on 26/04/2016.
 */
public class FiltreRGB extends Transformation{

    @Override
    public Image transformationImage(Image image){

        WritableImage wImage = new WritableImage((int)image.getWidth(), (int)image.getHeight());
        PixelReader pr = image.getPixelReader();
        PixelWriter pw = wImage.getPixelWriter();
        Color before;
        Color after;

        for(int x = 0; x < image.getWidth(); x++){

            for(int y = 0; y < image.getHeight(); y++){

                before = pr.getColor(x, y);
                after = new Color(before.getGreen(), before.getBlue(), before.getRed(), before.getOpacity());
                pw.setColor(x, y, after);

            }

        }

        return wImage;

    }

}
