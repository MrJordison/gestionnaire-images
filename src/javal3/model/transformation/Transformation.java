package javal3.model.transformation;

import javafx.scene.image.*;
import javafx.scene.image.Image;

/**
 * Created by Pierre on 26/04/2016.
 */
public abstract class Transformation {

    public abstract Image transformationImage(Image image);

}
