package javal3.model.transformation;

import javafx.scene.image.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * Created by Pierre on 26/04/2016.
 */
public class FiltreNoirBlanc extends Transformation {

    @Override
    public Image transformationImage(Image image){

        WritableImage wImage = new WritableImage((int)image.getWidth(), (int)image.getHeight());
        PixelReader pr = image.getPixelReader();
        PixelWriter pw = wImage.getPixelWriter();
        Color before;
        Color after;

        for(int x = 0; x < image.getWidth(); x++){

            for(int y = 0; y < image.getHeight(); y++){

                before = pr.getColor(x, y);
                double moy = 0.299*before.getRed()+0.57*before.getGreen()+0.114*before.getBlue();
                after = new Color(moy, moy, moy, before.getOpacity());
                pw.setColor(x, y, after);

            }

        }

        return wImage;

    }

}
