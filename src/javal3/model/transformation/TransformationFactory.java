package javal3.model.transformation;

/**
 * Created by Pierre on 03/05/2016.
 */
public class TransformationFactory {

    public Transformation getTransformation(String transfo){

        if(transfo.equals("RGB"))
            return new FiltreRGB();
        else if(transfo.equals("NoirBlanc"))
            return new FiltreNoirBlanc();
        else if(transfo.equals("Sepia"))
            return new FiltreSepia();
        else if(transfo.equals("Sobel"))
            return new FiltreSobel();
        else if(transfo.equals("Rotation"))
            return new TransformationRotation();
        else if(transfo.equals("RotationGauche"))
            return new TransformationRotation(90);
        else if(transfo.equals("RotationDroite"))
            return new TransformationRotation(-90);
        else if(transfo.equals("Rotation180"))
            return new TransformationRotation(180);
        else if(transfo.equals("SymetrieHorizontale"))
            return new TransformationSymetrie();
        else if(transfo.equals("SymetrieVerticale"))
            return new TransformationSymetrie(false);
        else if(transfo.equals("Securite"))
            return new FiltreSecurite();

        return null;

    }

}
