package javal3.model.transformation;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * Created by Pierre on 26/04/2016.
 */
public class TransformationRotation extends Transformation {

    private int angle;

    public TransformationRotation(){
        this.angle = 0;
    }

    public TransformationRotation(int angle){
        this.angle = angle;
    }

    @Override
    public Image transformationImage(Image image){

        WritableImage wImage;
        PixelReader pr = image.getPixelReader();
        Color before;

        if(angle == 90 || angle == -90)
            wImage = new WritableImage((int)image.getHeight(), (int)image.getWidth());
        else
            wImage = new WritableImage((int)image.getWidth(), (int)image.getHeight());

        PixelWriter pw = wImage.getPixelWriter();
        System.out.println(wImage.getWidth()+" ; "+wImage.getHeight());
        for(int x = 0; x < image.getWidth(); x++){

            for(int y = 0; y < image.getHeight(); y++){

                before = pr.getColor(x, y);
                int nextX = x;
                int nextY = y;

                switch(angle) {
                    case 90:
                        nextX = (int) image.getHeight() - y;
                        nextY = x;
                        break;
                    case -90:
                        nextY = (int) wImage.getHeight() - x;
                        nextX = y;
                        break;
                    case 180:
                        nextX = (int) image.getWidth() - x;
                        nextY = (int) image.getHeight() - y;
                        break;
                }

                if(nextX >= (int)wImage.getWidth())
                    nextX--;
                if(nextY >= (int)wImage.getHeight())
                    nextY--;

                pw.setColor(nextX, nextY, before);

            }

        }

        return wImage;

    }

    public int getAngle(){
        return this.angle;
    }

    public void setAngle(int angle){
        this.angle = angle;
    }

}
