package javal3.model.transformation;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javal3.Main;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by Pierre on 03/05/2016.
 */
public class FiltreSecurite extends Transformation {

    @Override
    public Image transformationImage(Image image) {

        WritableImage wImage = new WritableImage((int) image.getWidth(), (int) image.getHeight());
        PixelReader pr = image.getPixelReader();
        PixelWriter pw = wImage.getPixelWriter();
        Color before;
        Color after;

        SecureRandom sr = null;
        Random osr = null;
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("sha-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        boolean openjdk = System.getProperty("java.runtime.name").contains("OpenJDK");

        if (!openjdk){
            sr = new SecureRandom();

            sr.setSeed(md.digest(Main.password.getBytes()));
        }
        else{
            System.out.println("openjdk");
            osr = new Random();
            byte[] dig = md.digest(Main.password.getBytes());
            ByteBuffer bb = ByteBuffer.wrap(dig);
            osr.setSeed(bb.getLong());
        }

        int dx, dy;

        boolean verife[][] = new boolean[(int)image.getWidth()][(int)image.getHeight()];

        for(int x = 0; x < (int) image.getWidth(); x++){

            for(int y = 0; y < (int) image.getHeight(); y++){

                before = pr.getColor(x, y);

                if(!verife[x][y]){

                    do{
                        if(!openjdk) {
                            dx = sr.nextInt((int) image.getWidth());
                            dy = sr.nextInt((int) image.getHeight());
                        }
                        else{
                            dx = osr.nextInt((int) image.getWidth());
                            dy = osr.nextInt((int) image.getHeight());
                        }

                    }while(verife[dx][dy]);

                    verife[dx][dy] = true;
                    verife[x][y] = true;

                    after = pr.getColor(dx, dy);

                    pw.setColor(x, y, after);
                    pw.setColor(dx, dy, before);

                }

            }

        }

        return wImage;

    }

    public Image reconstituerImage(Image image, String mdp){

        WritableImage wImage = new WritableImage((int)image.getWidth(), (int)image.getHeight());
        PixelReader pr = image.getPixelReader();
        PixelWriter pw = wImage.getPixelWriter();
        Color before;
        Color after;

        SecureRandom sr = new SecureRandom();
        MessageDigest md = null;

        try {
            md = MessageDigest.getInstance("sha-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        sr.setSeed(md.digest(mdp.getBytes()));

        int dx, dy;

        boolean verife[][] = new boolean[(int)image.getWidth()][(int)image.getHeight()];

        for(int x = 0; x < image.getWidth(); x++){

            for(int y = 0; y < image.getHeight(); y++){

                before = pr.getColor(x, y);

                if(!verife[x][y]){

                    do{

                        dx = sr.nextInt((int)image.getWidth());
                        dy = sr.nextInt((int)image.getHeight());

                    }while(verife[dx][dy]);

                    verife[dx][dy] = true;
                    verife[x][y] = true;

                    after = pr.getColor(dx, dy);

                    pw.setColor(x, y, after);
                    pw.setColor(dx, dy, before);

                }

            }

        }

        return wImage;

    }

}
