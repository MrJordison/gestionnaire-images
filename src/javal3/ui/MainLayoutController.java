package javal3.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javal3.Main;
import javal3.model.Picture;
import org.controlsfx.control.GridCell;
import org.controlsfx.control.GridView;

import java.util.List;


public class MainLayoutController {

    @FXML private TextField searchField;
    @FXML private Button searchButton;

    @FXML private GridView<Picture> imagesDisplay;

    private ObservableList<Picture> allImageList;
    private ObservableList<Picture> imageslist;
    private Main monMain;
    private Stage stage;

    public MainLayoutController(){
        imagesDisplay = new GridView<>();
    }

    public void setMain(Main main){
        monMain = main;
    }
    public void setStage(Stage stage){
        this.stage = stage;
    }

    @FXML
    public void initialize(){
        //personnalise l'affichage d'une instance de Picture dans le gridview avec un template en fxml
        imagesDisplay.setCellFactory( cl -> {
            Node imgCell;
            ImageGridLayoutController controller;
            try {
                FXMLLoader loader =new FXMLLoader(getClass().getResource("ImageGridLayout.fxml"));
                imgCell = loader.load();
                controller = loader.getController();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            return new GridCell<Picture>() {
                @Override
                public void updateItem(Picture item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setGraphic(null);
                    } else {
                        controller.setPicture(item);
                        controller.setMain(monMain);
                        setGraphic(imgCell);
                    }
                }
            };
        });

    }

    public void setAllPicture(ObservableList<Picture> images){
        allImageList = images;
    }

    public void setPicturesList(ObservableList<Picture> images){
        imageslist = images;
        imagesDisplay.setItems(images);
    }

    @FXML
    public void searchSubmit(){
        String text = searchField.getText();

        ObservableList<Picture> obsImg = FXCollections.observableArrayList();;

        if(text.equals("") || text == null){

            List<Picture> img = monMain.getDAO().getAll();
            for(int i = 0; i < img.size(); i++)
                obsImg.add(allImageList.get(i));

        }
        else {

            for (int i = 0; i < allImageList.size(); i++) {

                Picture p = allImageList.get(i);
                if (p.getTagByNom(text) != null) {
                    obsImg.add(p);
                }
            }

        }

        setPicturesList(obsImg);

    }

    @FXML
    private void importHandle(){
        if(monMain.showImportImageDialog(stage)) {
            //TODO set focus sur l'image importée
        }
    }


}
