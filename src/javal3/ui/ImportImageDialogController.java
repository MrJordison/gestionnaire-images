package javal3.ui;

import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javal3.Main;
import javal3.model.Picture;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by mrjordison on 09/05/16.
 */
public class ImportImageDialogController {


    @FXML private TextField pathField;
    @FXML private TextField nameField;
    @FXML private Button validateButton;

    private Stage dialogStage;
    private ObservableList<Picture> imageslist;
    private boolean valid;
    private Main monMain;

    public ImportImageDialogController(){
        pathField = new TextField();
        nameField = new TextField();
        validateButton = new Button();
        valid = false;
    }

    @FXML
    private void initialize(){

        //désactive par défaut le bouton de validation
        validateButton.setDisable(true);

        //le bouton de validation se désactive si il n'y a pas de tag d'entré dans le champ de texte
        pathField.textProperty().addListener((obs,old,nouv) ->{
            validateButton.setDisable(nouv.length()==0);
        });
    }

    public void setStage(Stage stage){ dialogStage = stage; }
    public void setImagesList(ObservableList<Picture> l){
        imageslist = l;
    }
    public boolean isValid(){
        return valid;
    }
    public void setMain(Main main){ monMain = main;}

    @FXML
    private void handleValidate(){
        File source;
        //test si le chemin mentionné est correct
        if((source = new File(pathField.getText())).exists()){

            //si c'est le cas on instancie l'instance de Picture et on ferme la fenêtre de dialogue
            valid = true;

            //copie de l'image dans le dossier resources (dossier créé si non existant)

            String path[]= source.getPath().replace('\\','/').split("/");
            File dest = new File("resources/img/"+path[path.length-1]);
            dest.getParentFile().mkdirs();

            //TODO chiffrement de l'image stockée
            try {
                FileInputStream reader = new FileInputStream(source);
                FileOutputStream writer = new FileOutputStream(dest);

                byte[] buffer = new byte[1024];
                int taille;
                while((taille = reader.read(buffer)) > 0)
                    writer.write(buffer,0,taille);
                reader.close();
                writer.close();
            }
            catch(Exception e){
                monMain.showErrorExceptionDialog(e);
            }

            Picture p = new Picture(dest.toURI().toString(),
                    nameField.getText().length()==0 ? "" : nameField.getText());

            //maj du xml
            monMain.getDAO().create(p);

            imageslist.add(p);
            dialogStage.close();
        }
        else{
            //affichage d'une boite de dialogue d'erreur
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.initModality(Modality.WINDOW_MODAL);
            alert.setTitle("Erreur");
            alert.setHeaderText("Un problème est survenu à l'import de l'image");
            alert.setContentText("Le chemin vers le fichier est incorrect ou n'existe plus.");
            alert.showAndWait();
        }

    }

    @FXML
    private void handleChooserImport(){

        //la récupération du path de l'image se fait avec un FileChooser
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Import d'image");

        //passage des extensions acceptées de fichiers pour l'image
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Images", "*.png", "*.jpg", "*.jpeg"));
        File f = chooser.showOpenDialog(dialogStage);

        //après récupération d'une instance de File, si celui - ci n'est pas null, passage du path au champ de texte
        if(f!=null)
            pathField.setText(f.getAbsolutePath());
    }

    @FXML
    private void handleCancel(){
        dialogStage.close();
    }
}
