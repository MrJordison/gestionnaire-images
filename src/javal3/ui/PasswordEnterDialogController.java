package javal3.ui;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javal3.Main;
import javal3.model.Picture;

/**
 * Created by Pierre on 18/05/2016.
 */
public class PasswordEnterDialogController {

    @FXML
    private PasswordField passwordField;
    @FXML private Button validateButton;

    private Stage dialogStage;
    private Main monMain;

    public PasswordEnterDialogController(){
        passwordField = new PasswordField();
        validateButton = new Button();
    }

    @FXML
    private void initialize(){



    }

    public void setStage(Stage stage){ dialogStage = stage; }

    @FXML
    private void handleValidate(){
        boolean res;
        String msgerror="";
        if(passwordField.getText().length()!=0) {
            monMain.setPassword(passwordField.getText());
            res = true;
        }
        else {
            res = false;
            msgerror += "Le champ de texte pour le nom est vide";
        }
        if(res) {
            monMain.showMainLayout();
        }
        else{
            //affichage d'une boite de dialogue d'erreur
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.initModality(Modality.WINDOW_MODAL);
            alert.setTitle("Erreur");
            alert.setHeaderText("Un problème est survenu lors de la modification du nom");
            alert.setContentText(msgerror);
            alert.showAndWait();
        }

    }

    public void setMain(Main main){
        this.monMain = main;
    }

}
