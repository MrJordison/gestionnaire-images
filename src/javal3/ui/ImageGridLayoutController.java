package javal3.ui;

import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javal3.Main;
import javal3.model.Picture;

/**
 * Created by mrjordison on 02/05/16.
 */
public class ImageGridLayoutController {

    private Main monMain;

    @FXML private Label nameField;
    @FXML private ImageView thumbPicture;

    private Picture image;

    public ImageGridLayoutController(){
        nameField = new Label();
        thumbPicture = new ImageView();
        image = null;

    }

    @FXML
    public void initialize(){}

    public void setMain(Main main){
        monMain = main;
    }

    public void setPicture(Picture image){
        this.image = image;
        nameField.textProperty().bind(image.getNomProperty());
        thumbPicture.imageProperty().bind(image.getImageProperty());
    }

    @FXML
    public void handleClick(){
        monMain.showImageFullLayout(monMain.getImageslist().indexOf(image));
    }
}
