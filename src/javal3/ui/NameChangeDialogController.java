package javal3.ui;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javal3.model.Picture;

/**
 * Created by Pierre on 18/05/2016.
 */
public class NameChangeDialogController {

    @FXML
    private TextField nameField;
    @FXML private Button validateButton;

    private Stage dialogStage;
    private Picture picture;

    public NameChangeDialogController(){
        nameField = new TextField();
        validateButton = new Button();
    }

    @FXML
    private void initialize(){



    }

    public void setStage(Stage stage){ dialogStage = stage; }

    public void setPicture(Picture p){
        picture = p;
        nameField.setText(p.getNom());
    }

    @FXML
    private void handleValidate(){
        boolean res;
        String msgerror="";
        if(nameField.getText().length()!=0) {
            res = true;
            picture.setNom(nameField.getText());
        }
        else {
            res = false;
            msgerror += "Le champ de texte pour le nom est vide";
        }
        if(res)
            dialogStage.close();
        else{
            //affichage d'une boite de dialogue d'erreur
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.initModality(Modality.WINDOW_MODAL);
            alert.setTitle("Erreur");
            alert.setHeaderText("Un problème est survenu lors de la modification du nom");
            alert.setContentText(msgerror);
            alert.showAndWait();
        }

    }
    @FXML
    private void handleCancel(){
        dialogStage.close();
    }

}
