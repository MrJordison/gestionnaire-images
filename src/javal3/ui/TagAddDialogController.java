package javal3.ui;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javal3.Main;
import javal3.model.Picture;

/**
 * Created by mrjordison on 09/05/16.
 */
public class TagAddDialogController {

    @FXML private TextField tagField;
    @FXML private Button validateButton;

    private Stage dialogStage;
    private Picture picture;

    public TagAddDialogController(){
        tagField = new TextField();
        validateButton = new Button();
    }

    @FXML
    private void initialize(){

        //désactive par défaut le bouton de validation
        validateButton.setDisable(true);

        //le bouton de validation se désactive si il n'y a pas de tag d'entré dans le champ de texte
        tagField.textProperty().addListener((obs,old,nouv) ->{
            validateButton.setDisable(nouv.length()==0);
        });
    }

    public void setStage(Stage stage){ dialogStage = stage; }

    public void setPicture(Picture p){
        picture = p;
    }

    @FXML
    private void handleValidate(){
        boolean res;
        String msgerror="";
        if(tagField.getText().length()!=0) {
            if (!picture.addTag(tagField.getText())) {
                res = false;
                msgerror+= "Le tag spécifié est déjà présent dans la liste";
            } else
                res = true;
        }
        else {
            res = false;
            msgerror += "Le champ de texte pour le tag est vide";
        }
        if(res)
            dialogStage.close();
        else{
            //affichage d'une boite de dialogue d'erreur
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.initModality(Modality.WINDOW_MODAL);
            alert.setTitle("Erreur");
            alert.setHeaderText("Un problème est survenu à l'ajout du tag");
            alert.setContentText(msgerror);
            alert.showAndWait();
        }

    }
    @FXML
    private void handleCancel(){
        dialogStage.close();
    }
}
