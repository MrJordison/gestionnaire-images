package javal3.ui;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
;
import javafx.stage.Stage;
import javal3.Main;
import javal3.model.Picture;
import javal3.model.transformation.TransformationFactory;

/**
 * Created by mrjordison on 02/05/16.
 */
public class ImageFullLayoutController {

    @FXML private Button removeTagButton;
    @FXML private Button previousImageButton;
    @FXML private Button nextImageButton;
    @FXML private ImageView imageDisplay;
    @FXML private ListView<String> tagsDisplay;
    @FXML private Label imageNameField;
    @FXML private MenuItem selectAllTagMenu;
    @FXML private MenuItem deselectTagMenu;
    @FXML private MenuItem deleteTagMenu;

    private ObservableList<Picture> imageslist;
    private IntegerProperty index;
    private Picture image;
    private Stage imageDisplayStage;
    private Main monMain;
    private TransformationFactory transfact;

    private ChangeListener<String> nomListener;



    public ImageFullLayoutController(){
        removeTagButton = new Button();
        previousImageButton = new Button();
        nextImageButton = new Button();
        imageDisplay = new ImageView();
        tagsDisplay = new ListView<>();
        imageNameField = new Label("Nom de l'image");
        index = new SimpleIntegerProperty(-1);
        image = null;
        transfact = new TransformationFactory();
        selectAllTagMenu = new MenuItem();
        deleteTagMenu = new MenuItem();
        deselectTagMenu = new MenuItem();

        nomListener = (obs,old,nouv)-> imageDisplayStage.setTitle(imageslist.get(index.get()).getNom());
    }

    @FXML
    public void initialize(){

        //affiche un message par défaut si l'image n'a aucun tag
        tagsDisplay.setPlaceholder(new Label("Aucun tag à afficher"));


        //désactive également les boutons de suppression / selection si il n'y a aucun tag
        removeTagButton.setDisable(true);
        deleteTagMenu.setDisable(true);
        deselectTagMenu.setDisable(true);
        tagsDisplay.getItems().addListener((ListChangeListener<? super String>) listener->{
            if(tagsDisplay.getItems().size()==0) {
                removeTagButton.setDisable(true);
                deleteTagMenu.setDisable(true);
                selectAllTagMenu.setDisable(true);
                deselectTagMenu.setDisable(true);
            }
            else {
                removeTagButton.setDisable(false);
                deleteTagMenu.setDisable(false);
                selectAllTagMenu.setDisable(false);
                deselectTagMenu.setDisable(false);
            }
        });

        //ajout d'un listener sur la liste des tags qui désactive les boutons de suppression / selection  si aucun tag n'est sélectionné
        tagsDisplay.getSelectionModel().getSelectedItems().addListener((ListChangeListener<? super String>) listener->{
            if(tagsDisplay.getSelectionModel().getSelectedItems().size()==0) {
                removeTagButton.setDisable(true);
                deleteTagMenu.setDisable(true);
                deselectTagMenu.setDisable(true);
            }
            else {
                removeTagButton.setDisable(false);
                deleteTagMenu.setDisable(false);
                deselectTagMenu.setDisable(false);
            }
        });
        tagsDisplay.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        //ajout listener sur l'index pour mettre à jour les boutons "précédent" et "suivant"
        //se désactivent si il n'y a aucune ou une seule image dans la liste
        previousImageButton.setDisable(true);
        nextImageButton.setDisable(true);
        index.addListener((o,n,v)->{
            if(imageslist.size()!=0 && imageslist.size()!=1) {
                previousImageButton.setDisable(false);
                nextImageButton.setDisable(false);
            }
            else{
                previousImageButton.setDisable(true);
                nextImageButton.setDisable(true);
            }
        });
    }

    public void setStage(Stage stage){imageDisplayStage =stage;}

    public void setImages(ObservableList<Picture> list){
        imageslist = list;
    }

    public void setMain(Main main){
        monMain = main;
    }

    public void changeImageDisplay(int indice){
        //Suppression des listeners/ bind sur l'ancienne Picture si différente de null
        if(image!=null){
            image.getNomProperty().removeListener(nomListener);
            imageNameField.textProperty().unbind();
            imageDisplay.imageProperty().unbind();
        }

        index.set(indice);
        image = imageslist.get(indice);

        //Affichage de l'image, des tags et du nom dans l'interface
        tagsDisplay.setItems(image.getTags());

        imageDisplayStage.setTitle(imageslist.get(index.get()).getNom());

        //mise à jour des listeners/bind des élemens de l'interface sur la nouvelle instance de Picture
        image.getNomProperty().addListener(nomListener);
        imageNameField.textProperty().bind(image.getNomProperty());
        imageDisplay.imageProperty().bind(image.getImageProperty());


    }

    @FXML
    public void handleAddTag(){
        monMain.showTagAddDialog(imageDisplayStage,imageslist.get(index.get()));
        monMain.getDAO().update(image);
    }

    @FXML
    public void handleRemoveTag(){
        image.getTags().removeAll(tagsDisplay.getSelectionModel().getSelectedItems());
        monMain.getDAO().update(image);
    }

    @FXML
    public void handleChangeName(){
        monMain.showNameChangeDialog(imageDisplayStage, imageslist.get(index.get()));
        monMain.getDAO().update(image);
    }

    @FXML
    public void handlePreviousImage(){
        changeImageDisplay(Math.floorMod(index.getValue()-1,imageslist.size()));
    }

    @FXML
    public void handleNextImage(){
        changeImageDisplay(Math.floorMod(index.getValue()+1,imageslist.size()));
    }


    @FXML
    public void handleClose(){
        imageDisplayStage.close();
    }

    @FXML
    public void handleReset(){
        monMain.getDAO().deleteAllTransform(imageslist.get(index.get()));
        Picture np = monMain.getDAO().search(imageslist.get(index.get()).getId());
        imageslist.get(index.get()).setImg(np.getImage());
    }

    @FXML
    public void handleDeleteImage(){
        imageslist.remove(index.get());
        monMain.getDAO().delete(image);
        imageDisplayStage.close();
    }

    @FXML
    public void handleTransform(ActionEvent event){
        String transformation = ((MenuItem) event.getSource()).getId();
        imageslist.get(index.get()).applyTransformation(
                        transfact.getTransformation(transformation));
        monMain.getDAO().applyTransform(transformation,image);
    }

    @FXML
    public void handleAllSelectTag(){
        tagsDisplay.getSelectionModel().selectAll();
    }

    @FXML
    public void handleDeselectTag(){
        tagsDisplay.getSelectionModel().clearSelection();
    }

}
